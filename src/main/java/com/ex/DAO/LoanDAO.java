package com.ex.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.ex.Util.ConnFactory;
import com.ex.pojos.Loan;

public class LoanDAO {

	// private static ConnFactory connFactory = ConnFactory.getConnFactory();

	// queries

	private static final String ALL_LOANS = "select loan_id, amount, rate, bank_name, mbs_id, create_date, b.bank_id from loans l left join bank b on l.bank_id = b.bank_id order by loan_id";
	private static final String THIS_BANKS_LOANS = "select loan_id, amount, rate, bank_name, mbs_id, create_date, b.bank_id from loans l left join bank b on l.bank_id = b.bank_id where b.bank_id = ? order by loan_id";
	private static final String THIS_MBS_LOANS = "select loan_id, amount, rate, bank_name, mbs_id, create_date, b.bank_id from loans l left join bank b on l.bank_id = b.bank_id  where mbs_id=? order by bank_name, loan_id";
	private static final String BANKS_LOANS_FOR_MBS = "select loan_id, amount, rate, bank_name, mbs_id, create_date, b.bank_id from loans l left join bank b on l.bank_id = b.bank_id where l.bank_id=? and mbs_id is null order by bank_name, loan_id";
	private static final String NEW_LOAN = "insert into loans values(?,?,?,?,null,?)";
	private static final String EDIT_LOAN = "update loans set amount=?, rate=?, bank_id=? where loan_id=?";
	private static final String DELETE_LOAN = "delete from loans where loan_id=?";
	private static final String THIS_LOAN = "select loan_id, amount, rate, bank_name, mbs_id, create_date, b.bank_id from loans l left join bank b on l.bank_id = b.bank_id  where loan_id=? order by bank_name, loan_id";
	private static final String AVAILABLE_BANKS_LOANS = "select loan_id, amount, rate, bank_name, mbs_id, create_date, b.bank_id from loans l left join bank b on l.bank_id = b.bank_id where b.bank_id = ? AND l.mbs_id is null order by loan_id";

	public static List<Loan> getLoans() {
		List<Loan> loans = new ArrayList<Loan>();
		try {
			Connection conn = ConnFactory.getConnection();
			PreparedStatement pstmt = conn.prepareStatement(ALL_LOANS);

			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				Loan l = new Loan();
				l.setLoan_id(rs.getInt(1));
				l.setAmount(rs.getDouble(2));
				l.setRate(rs.getDouble(3));
				l.setBank_name(rs.getString(4));
				l.setCreated_date(rs.getDate(6));
				l.setMbs_id(rs.getInt(5));
				l.setBank_id(rs.getInt(7));

				loans.add(l);

			}

			pstmt.close();
			conn.close();

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return loans;
	}

	public static List<Loan> BanksLoans(int bank_id) {
		List<Loan> loans = new ArrayList<Loan>();
		try (Connection conn = ConnFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(THIS_BANKS_LOANS);) {
			pstmt.setInt(1, bank_id);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				Loan l = new Loan();
				l.setLoan_id(rs.getInt(1));
				l.setAmount(rs.getDouble(2));
				l.setRate(rs.getDouble(3));
				l.setBank_name(rs.getString(4));
				l.setCreated_date(rs.getDate(6));
				l.setMbs_id(rs.getInt(5));
				l.setBank_id(rs.getInt(7));
				loans.add(l);
			}

			pstmt.close();
			conn.close();

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return loans;
	}

	public static List<Loan> MBSLoans(int mbs_id) {
		List<Loan> loans = new ArrayList<Loan>();
		try (Connection conn = ConnFactory.getConnection();
			PreparedStatement pstmt = conn.prepareStatement(THIS_MBS_LOANS);) {
			pstmt.setInt(1, mbs_id);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				Loan l = new Loan();
				l.setLoan_id(rs.getInt(1));
				l.setAmount(rs.getDouble(2));
				l.setRate(rs.getDouble(3));
				l.setBank_name(rs.getString(4));
				l.setCreated_date(rs.getDate(6));
				l.setMbs_id(rs.getInt(5));
				l.setBank_id(rs.getInt(7));
				loans.add(l);
			}

			pstmt.close();
			conn.close();

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return loans;
	}

	public static List<Loan> BanksLoansForMBS(int bank_id) {
		List<Loan> loans = new ArrayList<Loan>();

		try (Connection conn = ConnFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(BANKS_LOANS_FOR_MBS);) {
			pstmt.setInt(1, bank_id);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				Loan l = new Loan();
				l.setLoan_id(rs.getInt(1));
				l.setAmount(rs.getDouble(2));
				l.setRate(rs.getDouble(3));
				l.setBank_name(rs.getString(4));
				l.setCreated_date(rs.getDate(6));
				l.setMbs_id(rs.getInt(5));
				l.setBank_id(rs.getInt(7));
				loans.add(l);
			}

			pstmt.close();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return loans;
	}

	public static java.util.Date generateDate(java.util.Date dMin, java.util.Date dMax) {
		long MILLIS_PER_DAY = 1000 * 60 * 60 * 24;
		GregorianCalendar s = new GregorianCalendar();
		s.setTimeInMillis(dMin.getTime());
		GregorianCalendar e = new GregorianCalendar();
		e.setTimeInMillis(dMax.getTime());

		// Get difference in milliseconds
		long endL = e.getTimeInMillis() + e.getTimeZone().getOffset(e.getTimeInMillis());
		long startL = s.getTimeInMillis() + s.getTimeZone().getOffset(s.getTimeInMillis());
		long dayDiff = (endL - startL) / MILLIS_PER_DAY;

		Calendar cal = Calendar.getInstance();
		cal.setTime(dMin);
		cal.add(Calendar.DATE, new Random().nextInt((int) dayDiff));
		return cal.getTime();
	}

	public static int NewLoan(double amount, double rate, int bank_id) {
		int new_id = 0;
		try {
			Connection conn = ConnFactory.getConnection();
			PreparedStatement pstmt = null;
			pstmt = conn.prepareStatement("select max(loan_id) from loans");
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			new_id = rs.getInt(1) + 1;

			pstmt = conn.prepareStatement(NEW_LOAN);
			
			Calendar cal = Calendar.getInstance();
	        cal.add(Calendar.MONTH, -8); // today minus one year
	        java.util.Date dMin = cal.getTime();
	        cal.add(Calendar.MONTH, 8); // today plus one year
	        java.util.Date dMax = cal.getTime();
	        
			java.util.Date date = generateDate(dMin, dMax);
			
			pstmt.setInt(1, new_id);
			pstmt.setDouble(2, amount);
			pstmt.setDouble(3, rate);
			pstmt.setInt(4, bank_id);
			pstmt.setDate(5, new Date(date.getTime()));
			pstmt.executeUpdate();

			pstmt.close();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new_id;

	}

	public static void EditLoan(double amount, double rate, int bank_id, int loan_id) {
		try (Connection conn = ConnFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(EDIT_LOAN);) {
			pstmt.setDouble(1, amount);
			pstmt.setDouble(2, rate);
			pstmt.setInt(3, bank_id);

			pstmt.setInt(4, loan_id);
			pstmt.executeUpdate();

			pstmt.close();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void DeleteLoan(int loan_id) {
		try (Connection conn = ConnFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(DELETE_LOAN);) {

			pstmt.setInt(1, loan_id);
			pstmt.executeUpdate();

			pstmt.close();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static Loan ThisLoan(int loan_id) {
		Loan l = new Loan();
		try (Connection conn = ConnFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(THIS_LOAN);) {
			pstmt.setInt(1, loan_id);
			ResultSet rs = pstmt.executeQuery();

			rs.next();

			l.setLoan_id(rs.getInt(1));
			l.setAmount(rs.getDouble(2));
			l.setRate(rs.getDouble(3));
			l.setBank_name(rs.getString(4));
			l.setCreated_date(rs.getDate(6));
			l.setMbs_id(rs.getInt(5));
			l.setBank_id(rs.getInt(7));

			pstmt.close();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return l;
	}

}
