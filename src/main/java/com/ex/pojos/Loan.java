package com.ex.pojos;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonObject;

public class Loan {
	
	private int loan_id;
	private double amount;
	private double rate;
	private String bank_name;
	private Date created_date;
	private int mbs_id;
	private int bank_id;
	
	public JsonObject toJSON(){
		SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
		JsonObject loan = new JsonObject();
		loan.addProperty("loan_id", this.loan_id);
		loan.addProperty("amount", this.amount);
		loan.addProperty("rate", this.rate);
		loan.addProperty("bank_id", this.bank_id);
		loan.addProperty("created_date", sdf.format(this.created_date));
		loan.addProperty("mbs_id", this.mbs_id);
		loan.addProperty("bank_name", this.bank_name);
		return loan;
		
	}
	
	
	@Override
	public String toString() {
		return "Loan [loan_id=" + loan_id + ", amount=" + amount + ", rate=" + rate + ", bank_name=" + bank_name
				+ ", created_date=" + created_date + ", mbs_id=" + mbs_id + "]";
	}
	public int getLoan_id() {
		return loan_id;
	}
	public void setLoan_id(int loan_id) {
		this.loan_id = loan_id;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public int getMbs_id() {
		return mbs_id;
	}
	public void setMbs_id(int mbs_id) {
		this.mbs_id = mbs_id;
	}
	
	public int getBank_id() {
		return bank_id;
	}
	public void setBank_id(int bank_id) {
		this.bank_id = bank_id;
	}
}
