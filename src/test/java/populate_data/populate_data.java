package populate_data;

import java.io.File; 
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ex.DAO.BankDAO;
import com.ex.pojos.Bank;

public class populate_data {
	private List<Bank> banks;
	private WebDriver driver;
	
	private static void wait(int miliSecond){
		try {
			Thread.sleep(miliSecond);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private double[] get_loan_values() {

		double amount = 0;
		double rate = 0;
		double bank_index = Math.random() * (double)banks.size();
		
		amount = (int) (5 + Math.random() * 95) * 10000;
		if(amount > 500000.0){
			rate = (int) ((73 + Math.random() * 27)) * 0.25;
			rate = Math.round(rate * 100.0) / 100.0;
		}
		else{
			rate = (int) ((1 + Math.random() * 71)) * 0.25;
			rate = Math.round(rate * 100.0) / 100.0;
		}
		double[] values = { amount, rate, bank_index };
		return values;
	}

	private void create_loans(int num_loans) {
		driver.findElement(By.xpath("//button[@value='new_loan']")).click();
		for(int i = 0; i < num_loans; i++){
			wait(500);
			double[] values = get_loan_values();
				
			driver.findElement(By.id("new_loan_amount")).sendKeys(Double.toString(values[0]));
			driver.findElement(By.id("new_loan_rate")).sendKeys(Double.toString(values[1]));
			
			Select banks_drop = new Select(driver.findElement(By.xpath("//select[@id='new_loan_banks']")));
			banks_drop.selectByIndex((int)values[2]);
			
			driver.findElement(By.id("new_loan_submit")).click();
		}

	}
	
	private void choose_smo(){
		int smo_index = (int)Math.random()*2;
		Select smo = new Select(driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/form[2]/table/tbody/tr[4]/td[2]/select")));
		smo.selectByIndex(smo_index);
		
	}
	
	private void quick_mbs(){
		driver.findElement(By.xpath("//button[@value='quick_submit_MBS']")).click();
		WebElement bank;
		for(int i = 1; i <= banks.size(); i++){
			bank = driver.findElement(By.xpath("//button[@value='"+ i +"'][@name='quick_mbs_bank']"));
			bank.click();
			double sum = Double.parseDouble(driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/form[2]/table/tbody/tr[2]/td[2]/input")).getAttribute("value"));
			WebElement submit = driver.findElement(By.xpath("//button[@name='quick_submit_mbs'][@value='"+ i +"']"));
			if(sum >= 3000000 && submit.isEnabled()){
				choose_smo();
				submit.click();
			}
		}
	}
	
	private void manual_mbs(){
		driver.findElement(By.xpath("//button[@value='manual_submit_MBS']")).click();
		List<WebElement> loans;
		WebElement bank;
		for(int i = 1; i <= banks.size(); i++){
			bank = driver.findElement(By.xpath("//button[@name='manual_mbs_bank'][@value='"+i+"']"));
			bank.click();
			
			wait(1000);
			
			loans = driver.findElements(By.name("manual_loan_list"));
			
			for(int j = 0; j < loans.size(); j++){
				
				loans.get(j).click();
				double sum = Double.parseDouble(driver.findElement(By.xpath("//input[@id='manual_mbs_sum_amount']")).getAttribute("value"));
				WebElement submit = driver.findElement(By.xpath("//*[@id='manual_submit']"));
				if(sum >= 3000000 && submit.isEnabled()){
					int smo_index = (int)Math.random()*2;
					Select smo = new Select(driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/form/table/tbody/tr[4]/td[2]/select")));
					smo.selectByIndex(smo_index);
					submit.click();
					wait(500);
					break;
				}
			}
			//System.out.println(loans.size());
		}
	}

	@Test(priority=1)
	public void test_quick_mbs() {
		System.out.println("@Test quick mbs");
		int num_loans = 10;
		int num_quick_mbs = 3;
		
		for(int i = 0; i < num_quick_mbs; i++){
			create_loans(num_loans);
			quick_mbs();
		}
	}
	
	@Test(priority=2)
	public void test_manual_mbs() {
		System.out.println("@Test manual mbs");
		int num_loans = 10;
		int num_manual_mbs = 3;
		
		for(int i = 0; i < num_manual_mbs; i++){
			create_loans(num_loans);
			manual_mbs();
		}
	}

	@Test(priority=3)
	public void test_approve_deny_mbs() {
		driver.findElement(By.xpath("/html/body/nav/div/ul/li[3]/a")).click();
		wait(500);
		driver.findElement(By.xpath("//button[@name='task'][@value='view_pending_mbs']")).click();
		
		wait(1000);
		
		List<WebElement> mbs_list = driver.findElements(By.xpath("//button[@name='loans']"));
		int num_mbs = mbs_list.size();
		int max_num = 10;
		
		while(num_mbs > 9 && max_num >= 0){
			
			mbs_list.get(0).click();
			int decision = (int)(Math.random()*3);
			
			wait(500);
			if(decision == 0){
				driver.findElement(By.id("denial_text")).sendKeys("Test for denying MBS");
				wait(500);
				WebElement deny = driver.findElement(By.id("denial_button")); 
				if(deny.isEnabled())
					deny.click();
				
			}
			else{
				driver.findElement(By.id("approve_button")).click();
				wait(500);
				
			}
			
			driver.findElement(By.xpath("//button[@name='task'][@value='view_pending_mbs']")).click();
			wait(1000);
			mbs_list = driver.findElements(By.xpath("//button[@name='loans']"));
			num_mbs = mbs_list.size();
			max_num--;
		}
		
		
	}
	
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("@Befrore Method");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("@After Method");
	}

	@DataProvider
	public Object[][] dp() {
		return new Object[][] { new Object[] { 1, "a" },

		};
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("@Before Class");
		banks = BankDAO.getBanks();
		
		File file = new File("C:/Jar_folder/selenium-2.53.1/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://localhost:7001/DreamWalker/bank.jsp");
	}

	@AfterClass
	public void afterClass() {
		System.out.println("@After Class");
		driver.quit();
	}

	@BeforeTest
	public void beforeTest() {
		System.out.println("@Before Test");

		
	}

	@AfterTest
	public void afterTest() {
		System.out.println("@After Test");
	}

	@BeforeSuite
	public void beforeSuite() {
		System.out.println("@Before Suite");
	}

	@AfterSuite
	public void afterSuite() {
		System.out.println("@After Suite");
	}

}
