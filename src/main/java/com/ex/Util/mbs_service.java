package com.ex.Util;


import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ex.DAO.LoanDAO;
import com.ex.DAO.MBSDAO;
import com.ex.pojos.Loan;
import com.ex.pojos.MBS;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


public class mbs_service extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public mbs_service() {
        super();
    }

    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		System.out.println("");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("get_mbs_json") != null){
			int mbs_id = Integer.parseInt(request.getParameter("get_mbs_json"));
			MBS mbs = MBSDAO.ThisMBS(mbs_id);
			mbs.setLoans(LoanDAO.MBSLoans(mbs_id));
			JsonObject json = mbs.toJSON();
			response.setContentType("application/json");
			response.getWriter().write(json.toString());
			
		}
		
		if(request.getParameter("denied_mbs_json") != null){
			int mbs_id = Integer.parseInt(request.getParameter("denied_mbs_json"));
			MBS mbs = MBSDAO.ThisMBS(mbs_id);
			mbs.setLoans(LoanDAO.MBSLoans(mbs_id));
			JsonObject json = mbs.toJSON();
			
			List<Loan> loans = LoanDAO.BanksLoansForMBS(mbs.getBank_id());
			JsonArray loan_array = new JsonArray();
			for(Loan l: loans){
				loan_array.add(l.toJSON());
			}
			json.add("available_loans", loan_array);
			
			
			response.setContentType("application/json");
			response.getWriter().write(json.toString());
		}
		
		if(request.getParameter("approve") != null){
			MBSDAO.Approved(Integer.parseInt(request.getParameter("approve")));
			response.sendRedirect("smo.jsp?approved="+request.getParameter("approve"));
		}
		
		if(request.getParameter("deny")!=null){
			MBSDAO.Denied(request.getParameter("reason"), Integer.parseInt(request.getParameter("deny")));
			response.sendRedirect("smo.jsp?denied="+request.getParameter("deny"));
		}
		
		if(request.getParameter("quick_submit_mbs") != null){
			int mbs_id = MBSDAO.NewMBS(Integer.parseInt(request.getParameter("quick_submit_mbs")),
				 Double.parseDouble(request.getParameter("sum")),
				 Double.parseDouble(request.getParameter("avg")),
				 Integer.parseInt(request.getParameter("num_loans")), 
				 request.getParameter("SMO"));
			response.sendRedirect("bank.jsp?quick_mbs_submitted=" + mbs_id);
		}
		
		if(request.getParameter("manual_submit_mbs") != null){
			String[] loan_id_list = request.getParameterValues("manual_loan_list");
			int mbs_id = MBSDAO.NewManualMBS(Integer.parseInt(request.getParameter("manual_submit_mbs")), 
					Double.parseDouble(request.getParameter("manual_mbs_sum_amount")),
					Double.parseDouble(request.getParameter("manual_mbs_avg_rate")),
					Integer.parseInt(request.getParameter("manual_mbs_num_loans")),  
					request.getParameter("SMO"), 
					loan_id_list);
			response.sendRedirect("bank.jsp?manual_mbs_submitted=" + mbs_id);
		}
		
		if(request.getParameter("denied_mbs_submit") != null){
			int mbs_id = Integer.parseInt(request.getParameter("denied_mbs_submit"));
			String[] loans = request.getParameterValues("chosen_loans");
			MBSDAO.submit_denied_mbs(Integer.parseInt(request.getParameter("denied_mbs_num_loans")),
					Double.parseDouble(request.getParameter("denied_mbs_sum_amount")),
					Double.parseDouble(request.getParameter("denied_mbs_avg_rate")),
					request.getParameter("SMO"),
					mbs_id, loans);
			
			response.sendRedirect("bank.jsp?denied_mbs_submit=" + mbs_id);
		}
		
		if(request.getParameter("denied_mbs_save") != null){
			int mbs_id = Integer.parseInt(request.getParameter("denied_mbs_save"));
			String[] loans = request.getParameterValues("chosen_loans");
			MBSDAO.save_denied_mbs(Integer.parseInt(request.getParameter("denied_mbs_num_loans")),
					Double.parseDouble(request.getParameter("denied_mbs_sum_amount")),
					Double.parseDouble(request.getParameter("denied_mbs_avg_rate")),
					request.getParameter("SMO"),
					mbs_id, loans);
			response.sendRedirect("bank.jsp?denied_mbs_save=" + mbs_id);
		}
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
	}

}
