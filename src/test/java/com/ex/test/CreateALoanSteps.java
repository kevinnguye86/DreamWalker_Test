package com.ex.test;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.ex.DAO.BankDAO;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateALoanSteps {

	public static WebDriver driver;

	@Given("^A banker is on the create new loan page$")
	public void a_banker_is_on_the_create_new_loan_page() throws Throwable {
		driver = getDriver();
	}

	@Given("^the banker clicks on the new loan tab$")
	public void the_banker_clicks_on_the_new_loan_tab() throws Throwable {
		driver.findElement(By.xpath("//button[@value='new_loan']")).click();
	}

	@When("^banker input a \"([^\"]*)\" in Amount$")
	public void banker_input_a_in_Amount(String amount) throws Throwable {
		driver.findElement(By.id("new_loan_amount")).sendKeys(amount);
	}

	@When("^input a \"([^\"]*)\" in Interest Rate$")
	public void input_a_in_Interest_Rate(String rate) throws Throwable {
		driver.findElement(By.id("new_loan_rate")).sendKeys(rate);
	}

	@When("^choose \"([^\"]*)\" as my bank from the drop down menu$")
	public void choose_as_my_bank_from_the_drop_down_menu(String name) throws Throwable {
		int id = BankDAO.getId(name);

		Select banks = new Select(driver.findElement(By.id("new_loan_banks")));
		banks.selectByIndex(id - 1);
	}

	@When("^click the add loan button$")
	public void click_the_add_loan_button() throws Throwable {
		driver.findElement(By.id("new_loan_submit")).click();
	}

	@Then("^A new loan \"([^\"]*)\"$")
	public void a_new_loan(String result) throws Throwable {

		if (result.equals("can submit")) {
			Assert.assertEquals(driver.findElement(By.id("new_loan_amount")).getAttribute("value"), "");
			Assert.assertEquals(driver.findElement(By.id("new_loan_rate")).getAttribute("value"), "");
		} else {
			Assert.assertNotEquals(driver.findElement(By.id("new_loan_amount")).getAttribute("value"), "");
			Assert.assertNotEquals(driver.findElement(By.id("new_loan_rate")).getAttribute("value"), "");
		}
		
	}
	
	@Given("^those steps are done$")
	public void those_steps_are_done() throws Throwable {
	}

	@Then("^close the web driver for creating loan$")
	public void quit_the_web_driver() throws Throwable {
		driver.quit();
		driver = null;
	}
	

	public static WebDriver getDriver() {
		if (driver == null) {
			File file = new File("C:\\Jar_folder\\selenium-2.53.1\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
			driver = new ChromeDriver();
			driver.get("http://localhost:7001/DreamWalker/bank.jsp");
		}
		return driver;
	}

}