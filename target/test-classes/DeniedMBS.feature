Feature: As a banker
  I want to manage my denied MBS
  and be able to resubmit them.

  Background: check if there is denied mbs
    Given banker clicks denied MBS button

  Scenario Outline: just resubmit the MBS with no changes
    And they click their "<bank>"
    When they have denied MBS
    And the click view details
    And click submit MBS
    Then that MBS status will be changed to pending

    Examples: 
      | bank            |
      | Chase           |
      | Bank of America |
      | Capital One     |
      | PNC             |
      | Wells Fargo     |

  Scenario Outline: if a total amount drops below three million do not submit
    And they click their "<bank>"
    When they have denied MBS
    And the click view details
    And the loans are removed until below three million
    And click submit MBS
    Then that MBS will not be able to submit

    Examples: 
      | bank            |
      | Chase           |
      | Bank of America |
      | Capital One     |
      | PNC             |
      | Wells Fargo     |

  Scenario Outline: after removing a loan the MBS click save
    And they click their "<bank>"
    When they have denied MBS
    And the click view details
    And a loan is removed
    And click save
    Then the loan that was removed will have its mbs_id changed to null

    Examples: 
      | bank            |
      | Chase           |
      | Bank of America |
      | Capital One     |
      | PNC             |
      | Wells Fargo     |

  Scenario Outline: after adding a loan the MBS click save
    And they click their "<bank>"
    When they have denied MBS
    And the click view details
    And a loan is added
    And click save
    Then the loan that was added will have its mbs_id changed to the denied MBS

    Examples: 
      | bank            |
      | Chase           |
      | Bank of America |
      | Capital One     |
      | PNC             |
      | Wells Fargo     |

  Scenario Outline: after adding a loan the MBS click submit
    And they click their "<bank>"
    When they have denied MBS
    And the click view details
    And a loan is added until at three million
    And click submit
    Then the loan that was added will have its mbs_id changed to the denied MBS and then it will changed to pending

    Examples: 
      | bank            |
      | Chase           |
      | Bank of America |
      | Capital One     |
      | PNC             |
      | Wells Fargo     |

  Scenario: Assume all the scenario was tested
    Then close the web driver for denied_mbs
