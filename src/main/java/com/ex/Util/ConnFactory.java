package com.ex.Util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnFactory {

	private static ConnFactory connFactory;

	private ConnFactory() {}

	public static ConnFactory getConnFactory() {
		if (connFactory == null) {
			connFactory = new ConnFactory();
		}
		return connFactory;
	}

	public static Connection getConnection() throws SQLException {
		
		Connection conn = null;
        try {
        	//String databaseURL = "jdbc:mysql://www.op3gmu.com:3306/opthrgmu_Loan";
        	//Class.forName("com.mysql.jdbc.Driver").newInstance();
        	String databaseURL = "jdbc:oracle:thin:@localhost:1521:xe";
            Class.forName("oracle.jdbc.OracleDriver");
            
            Properties properties = new Properties();
            //properties.setProperty("user", "opthrgmu_kevin");
            properties.setProperty("user", "dreamwalker");
            properties.setProperty("password", "password");
            
            conn = DriverManager.getConnection(databaseURL, properties);
        } catch (Exception e) {
            System.out.println("Connection Exception");
            e.printStackTrace();
        }
        
        return conn;
	}

}
