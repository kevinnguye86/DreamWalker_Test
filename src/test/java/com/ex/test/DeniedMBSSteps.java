package com.ex.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.ex.DAO.BankDAO;
import com.ex.DAO.LoanDAO;
import com.ex.DAO.MBSDAO;
import com.ex.pojos.Loan;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DeniedMBSSteps {

	private static WebDriver driver;
	private static WebDriverWait wait;
	boolean hasDenied = false;
	int MBS_id; // this needs to be used to check if the mbs changes states
	List<Integer> loan_id = new ArrayList<Integer>(); // also needs to be
														// checked for change in
														// MBS_id for
	// removed/added elements
	// reset the list to null whenever u finish a scenario so it doesnt carry
	// over

	// all tests start the same getting to the details of a page
	@Given("^banker clicks denied MBS button$")
	public void banker_clicks_denied_MBS_button() throws Throwable {
		// Zachary may have found a way to make it only one browser but until
		// test works not gonna worry about it
		
		driver = getDriver();
		driver.findElement(By.xpath("//button[@value='denied_MBS']")).click();

	}

	@Given("^they click their \"([^\"]*)\"$")
	public void they_click_their(String bank) throws Throwable {

		driver.findElement(By.xpath("//button[@value='" + BankDAO.getId(bank) + "']")).click();

	}

	@When("^they have denied MBS$")
	public void they_have_denied_MBS() throws Throwable {

		hasDenied = driver.findElements(By.name("denied_mbs_fill")).size() != 0;
		// this statement allows the program to know if there are denied mbs for
		// that bank
		// it is useful as it will allow for the skiping of code in later steps
		// if its got no denied MBS
	}

	@When("^the click view details$")
	public void the_click_view_details() throws Throwable {
		if (hasDenied) {
			// this seems an odd place to get the mbs but it is only time mbs_id
			// is used
			MBS_id = Integer.parseInt(driver.findElements(By.name("denied_mbs_fill")).get(0).getAttribute("value"));
			driver.findElements(By.name("denied_mbs_fill")).get(0).click();
			// if need be we could maybe expand this to include a test for all
			// mbs of a bank
			// however right now its only being done on the first one
			WebElement element = driver.findElement(By.id("denied-mbs-sum-amount"));
			// wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("denied_mbs_submit")));
			// wait.until(ExpectedConditions.attributeContains(By.id("denied_mbs_submit"),
			// "disabled", "false"));
			wait.until(ExpectedConditions.attributeToBeNotEmpty(element, "value"));
			for (int i = 0; i < 100000000; i++) {
			}

		}

	}

	// everything above is a repeated part
	// used for scenario 1 and 2
	@When("^click submit MBS$")
	public void click_submit_MBS() throws Throwable {

		if (hasDenied) {

			driver.findElement(By.id("denied_mbs_submit")).click();

		}
	}

	@Then("^that MBS status will be changed to pending$")
	public void that_MBS_status_will_be_changed_to_pending() throws Throwable {

		if (hasDenied) {
			Assert.assertEquals(MBSDAO.ThisMBS(MBS_id).getStatus(), "pending");
		}
		
	}

	// scenario 2
	@When("^the loans are removed until below three million$")
	public void the_loans_are_removed_until_below_three_million() throws Throwable {
		if (hasDenied) {
			if(driver.findElements(By.id("added_loanss")).size() != 0)
			{
			String s = driver.findElement(By.id("denied_mbs_sum_amount")).getAttribute("value");
			int sum = Integer.parseInt(s);
			
			while (sum >= 3000000) {
				// i have changed the script.js to include a value and id for
				// each of the buttons on
				// added_loans and available loans
				s = driver.findElement(By.id("denied_mbs_sum_amount")).getAttribute("value");

				// System.out.println(Integer.parseInt(driver.findElements(By.id("added_loanss")).get(0).getAttribute("name")));
				loan_id.add(Integer.parseInt(driver.findElements(By.id("added_loanss")).get(0).getAttribute("name")));
				// this will be used to check that they indeed were changed
				// however this test wont need it since it is testing that it
				// cant click the submit button
				driver.findElements(By.id("added_loanss")).get(0).click();
				s = driver.findElement(By.id("denied_mbs_sum_amount")).getAttribute("value");
				sum = Integer.parseInt(s);
				
				}
			}
		}
	}

	@Then("^that MBS will not be able to submit$")
	public void that_MBS_will_not_be_able_to_submit() throws Throwable {
		if (hasDenied) {
			Assert.assertEquals(driver.findElement(By.id("denied_mbs_submit")).isEnabled(), false);
		}
		loan_id = new ArrayList<Integer>();
		

	}
	
	
//--------------------------------------------------------
	// scenario 3
	@When("^a loan is removed$")
	public void a_loan_is_removed() throws Throwable {
		if (hasDenied) {
			// i have changed the script.js to include a value and id for each
			// of the buttons on
			// added_loans and available loans
			
			loan_id.add(Integer.parseInt(driver.findElements(By.id("added_loanss")).get(0).getAttribute("name")));
			// this will be used to check that they indeed were changed
			if(driver.findElements(By.id("added_loanss")).size() != 0)
			{
			driver.findElements(By.id("added_loanss")).get(0).click();
			}
		}

	}

	// this is a repeated step for a few scenarios 3 and 4
	@When("^click save$")
	public void click_save() throws Throwable {
		if (hasDenied) {
			driver.findElement(By.id("denied_mbs_save")).click();

		}

	}

	@Then("^the loan that was removed will have its mbs_id changed to null$")
	public void the_loan_that_was_removed_will_have_its_mbs_id_changed_to_null() throws Throwable {
		List<Loan> loans = LoanDAO.MBSLoans(MBS_id); 
		if (hasDenied) {
			for (Loan l : loans) {
				Assert.assertNotEquals(l.getLoan_id(), loan_id.get(0));// Only work with one
																		
			}
		}
		loan_id = new ArrayList<Integer>();
		

	}

	// start scenario 4
	// this is used in 4 and 5
	@When("^a loan is added$")
	public void a_loan_is_added() throws Throwable {
		if (hasDenied) {
			// i have changed the script.js to include a value and id for each
			// of the buttons on
			// added_loans and available loans
			if(driver.findElements(By.id("available_loanss")).size() != 0)
			{
			loan_id.add(Integer.parseInt(driver.findElements(By.id("available_loanss")).get(0).getAttribute("name")));
			// this will be used to check that they indeed were changed
			}	
			driver.findElements(By.id("available_loanss")).get(0).click();

		}

	}

	// might need to come back to these sections they only check if it was
	// removed from that mbs not where it goes
	@Then("^the loan that was added will have its mbs_id changed to the denied MBS$")
	public void the_loan_that_was_added_will_have_its_mbs_id_changed_to_the_denied_MBS() throws Throwable {
		boolean wasAdded = false;
		if (hasDenied) {
			for (Loan l : LoanDAO.MBSLoans(MBS_id)) {
				if (l.getLoan_id() == loan_id.get(0)) // check if the added loan
														// had its mbs_id
														// changed
				{
					wasAdded = true;
				}
			}

			Assert.assertEquals(wasAdded, true);
		}
		loan_id = new ArrayList<Integer>();
		

	}

	// start scenario 5

	@When("^click submit$")
	public void click_submit() throws Throwable {
		if (hasDenied) {
			driver.findElement(By.id("denied_mbs_submit")).click();

		}

	}

	@When("^a loan is added until at three million$")
	public void a_loan_is_added_until_at_three_million() throws Throwable {
		String s;
		int sum;
		if (hasDenied) {
			if(driver.findElements(By.id("available_loanss")).size() != 0)
			{
			// i have changed the script.js to include a value and id for each
			// of the buttons on
			// added_loans and available loans
			do {
				
				loan_id.add(
						Integer.parseInt(driver.findElements(By.id("available_loanss")).get(0).getAttribute("name")));
				// this will be used to check that they indeed were changed

				driver.findElements(By.id("available_loanss")).get(0).click();
				s = driver.findElement(By.id("denied_mbs_sum_amount")).getAttribute("value");
				sum = Integer.parseInt(s);
				
			} while (sum < 3000000);
			}
		}

	}

	@Then("^the loan that was added will have its mbs_id changed to the denied MBS and then it will changed to pending$")
	public void the_loan_that_was_added_will_have_its_mbs_id_changed_to_the_denied_MBS_and_then_it_will_changed_to_pending()
			throws Throwable {
		boolean wasAdded = false;
		if (hasDenied) {
			for (Loan l : LoanDAO.MBSLoans(MBS_id)) {
				if(loan_id.size() !=0){
					if (l.getLoan_id() == loan_id.get(0)) // check if the added loan
															// had its mbs_id
															// changed
					{
						wasAdded = true;
					}
				}
			}
			if(loan_id.size() !=0)
				Assert.assertEquals(wasAdded, true);
			Assert.assertEquals(MBSDAO.ThisMBS(MBS_id).getStatus(), "pending");
			
			// have to check both to make sure both actions work together.
		}
		loan_id = new ArrayList<Integer>();

	}

	@Then("^close the web driver for denied_mbs$")
	public void close_the_web_driver() throws Throwable {
		driver.quit();
	}

	public static WebDriver getDriver() {
		if (driver == null) {
			File file = new File("C:\\Jar_folder\\selenium-2.53.1\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
			driver = new ChromeDriver();
			driver.get("http://localhost:7001/DreamWalker/bank.jsp");
		}
		wait = new WebDriverWait(driver, 30);
		return driver;
	}
}
