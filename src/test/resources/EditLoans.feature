Feature: Edit Loans
  As a banker 
  I want to be able to chose a loan
  and edit all the fields except for the loan id

  Scenario Outline: A banker tries to edit a loan information
    Given A banker is on the view all loans page
    And the banker clicks on the edit loan button
    When banker input "<amount>", "<rate>" in edit fields
    And click the save button
    Then the loan should not be saved

    Examples: 
      | amount  | rate  |
      |         |  3.25 |
      |   49999 |  3.25 |
      | 3000001 |  3.25 |
      |  320000 |       |
      |  320000 |     0 |
      |  320000 |  10.3 |
      |  320000 | 25.25 |

  Scenario: Assume all the scenario was tested
    Then close the web driver for edit loan
