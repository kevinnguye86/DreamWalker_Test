package com.ex.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ex.Util.ConnFactory;
import com.ex.pojos.Bank;

public class BankDAO {
	
	//private ConnFactory connFactory = ConnFactory.getConnFactory();
	
	
	private static final String GET_NAMES = "select * from bank order by bank_id";
	private static final String GET_PC ="select payee_code from bank where bank_id=?";
	private static final String GET_BANK ="select * from bank where bank_id=?";
	private static final String GET_ID ="select bank_id from bank where bank_name=?";
	
	public static int getId(String bank_name){
		int id = 0;
		try(Connection conn = ConnFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(GET_ID);)
		{
			pstmt.setString(1, bank_name);
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()){
				id = rs.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
				
	}
	
	public static Bank getBank(int bank_id){
		Bank b = new Bank();
		try(Connection conn = ConnFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(GET_BANK);)
		{
			pstmt.setInt(1, bank_id);
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()){
				b.setBank_id(rs.getInt(1));
				b.setName(rs.getString(2));
				b.setPayee_code(rs.getString(3));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
				
	}
	
	public static List<Bank> getBanks()
	{
		List<Bank> banks = new ArrayList<Bank>();
		
		try(Connection conn = ConnFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(GET_NAMES);)
		{
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				Bank b = new Bank();
				b.setBank_id(rs.getInt(1));
				b.setName(rs.getString(2));
				b.setPayee_code(rs.getString(3));
				banks.add(b);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return banks;
				
	}
	
	public static String GetPC()
	{
		String PC="";
		
		try(Connection conn = ConnFactory.getConnection();
				PreparedStatement pstmt = conn.prepareStatement(GET_PC);)
		{
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			PC=rs.getString(1);
				
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return PC;
				
	}
	
	
	

}
