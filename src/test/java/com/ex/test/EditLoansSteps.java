package com.ex.test;
import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class EditLoansSteps {
	private static WebDriver driver;
	
	@Given("^A banker is on the view all loans page$")
	public void a_banker_is_on_the_view_all_loans_page() throws Throwable {
		driver = getDriver();
		if(driver.findElement(By.id("edit_loan_div")).isDisplayed()){
			driver.findElement(By.xpath("//*[@id='edit_loan_div']/div/div/div[3]/button")).click();
			wait(300);
		}
		driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/button[2]")).click();
		
	}

	@Given("^the banker clicks on the edit loan button$")
	public void the_banker_clicks_on_the_edit_loan_button() throws Throwable {
		driver.findElement(By.xpath("//*[@id='all_loans_table']/thead/tr/th[6]")).click();
		
		List<WebElement> edit_buttons = driver.findElements(By.name("edit"));
		
		if(edit_buttons.size() > 0 && edit_buttons.get(0).isEnabled())
			edit_buttons.get(0).click();
		wait(300);
	}

	@When("^banker input \"([^\"]*)\", \"([^\"]*)\" in edit fields$")
	public void banker_input_and_in_edit_fields(String amount, String rate) throws Throwable {
		WebElement amount1 = driver.findElement(By.id("edit_loan_amount"));
		WebElement rate1 = driver.findElement(By.id("edit_loan_rate"));
		amount1.clear();
		rate1.clear();
		amount1.sendKeys(amount);
		rate1.sendKeys(rate);
		
		
	}

	@When("^click the save button$")
	public void click_the_save_button() throws Throwable {
		driver.findElement(By.xpath("//button[@id='commit_edit_loan']")).click();
		//wait(1000);
	}

	@Then("^the loan should not be saved$")
	public void the_loan_should_not_be_saved() throws Throwable {
		Assert.assertEquals(driver.findElement(By.id("edit_loan_div")).isDisplayed(), true);
		
	}
	
	@Then("^close the web driver for edit loan$")
	public void close_the_web_driver() throws Throwable {
		driver.quit();
	}
	
	public static WebDriver getDriver() {
		if (driver == null) {
			File file = new File("C:\\Jar_folder\\selenium-2.53.1\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
			driver = new ChromeDriver();
			driver.get("http://localhost:7001/DreamWalker/bank.jsp");
		}
		return driver;
	}
	
	private static void wait(int miliSecond){
		try {
			Thread.sleep(miliSecond);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
