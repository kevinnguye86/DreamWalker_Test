Feature: Create a Loan
  As a Banker
  I want to be able to create a loan
  in order to add it to a MSB to be sent to the SMO

  Scenario Outline: A banker tries to submit loan information
    Given A banker is on the create new loan page
    And the banker clicks on the new loan tab
    When banker input a "<amount>" in Amount
    And input a "<rate>" in Interest Rate
    And choose "<bank>" as my bank from the drop down menu
    And click the add loan button
    Then A new loan "<expected_outcome>"

    Examples: 
      | amount  | rate  | bank  | expected_outcome |
      |  120000 |  -1.0 | Chase | can not submit   |
      |  350000 |   0.0 | Chase | can not submit   |
      |  400000 |  0.25 | Chase | can submit       |
      |  320000 |  25.0 | Chase | can submit       |
      |  550000 | 25.25 | Chase | can not submit   |
      |   49999 |  5.25 | Chase | can not submit   |
      |   50000 | 10.50 | Chase | can submit       |
      | 2000000 | 12.50 | Chase | can submit       |
      | 3000000 | 22.25 | Chase | can submit       |
      | 3000001 | 20.75 | Chase | can not submit   |
      
    Scenario: Close the web driver
    Given those steps are done
    Then close the web driver for creating loan
